========================
Django deployment script
========================

This package provides a fabric script for building and maintaining django 
web application hosting. The goal is to simplify the deploy process.

Keep in mind, that this script ist still under construction. If you have found
a bug or you have a suggestion, contact me or fire an issue or send a pull
request.

Source code: https://bitbucket.org/ses_schulz/django-deployment/src

Bug tracker: https://bitbucket.org/ses_schulz/django-deployment/issues/

Requirements
============

* Fabric >= 1.4.0
* Django >= 1.3.1

Installation
============

::

    pip install git+ssh://git@bitbucket.org/ses_schulz/django-deployment.git


Usage
=====

Create in the root directory of your project a ``fabfile.py``.

Example::

    from fabric.api import env, task
    import deploy

    @task
    def config_test():
        env.hosts = ['example.com', ]
        env.user = "username"
        env.VIRTUAL_ENV = "/home/dir-of-project"
        env.VCS_URL = 'http://example.com/my-git-repo'
        env.DJANGO_SETTINGS_MODULE = 'settings'
        env.LOCAL_SETTINGS = 'config/local.py'
        env.REMOTE_SETTINGS = '%(VIRTUAL_ENV)s/src/settings/local.py' % env
        env.DJANGO_FCGI_PID = '%(VIRTUAL_ENV)s/django-fcgi.pid' % env

Now you can deploy your django project the first time.

Example::

    fab config_test deploy.first_deploy

New deployments, you can realize with this command

Example::

    fab config_test deploy


