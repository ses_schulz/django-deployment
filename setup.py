from distutils.core import setup

setup(
    name="django-deployment",
    version="0.1",
    py_modules=['deploy'],

    install_requires=['Fabric>=1.4.0'],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
      #  '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
     #   'hello': ['*.msg'],
    },

    # metadata for upload to PyPI
    author="Sebastian Schulz",
    author_email="ses.bitbucket@sebatec.eu",
    description="This package provides a fabric script for building and maintaining "\
                "django web application hosting.",
    license='MIT license',
    keywords="virtual machine server hosting management script",
    url="http://bitbucket.org/ses_schulz/django-deployment/",
    download_url='https://bitbucket.org/ses_schulz/django-deployment/get/master.zip',

    # could also include long_description, download_url, classifiers, etc.
)
