from fabric.api import task, run, env, cd, require
from fabric.context_managers import prefix, settings
from fabric.contrib import django
from fabric.contrib.files import exists
from fabric.operations import put
from fabric.utils import abort
from contextlib import nested


def activate_virtualenv():
    cd(env.VIRTUAL_ENV)
    return nested(
        cd(env.VIRTUAL_ENV), 
        prefix('source %(VIRTUAL_ENV)s/bin/activate' % env),
    )


@task 
def check():
    "check if all variables are set."
    require('hosts')
    require('user')
    require('VIRTUAL_ENV')
    require('VCS_URL')
    require('DJANGO_SETTINGS_MODULE')
    require('LOCAL_SETTINGS')
    require('REMOTE_SETTINGS')
    require('DJANGO_FCGI_PID')
    if not exists(env.VIRTUAL_ENV):
        abort('execute first deploy!')

@task
def first_deploy():
    "start this task to deploy the first time."
    if exists(env.VIRTUAL_ENV):
        return
    run('git clone %(VCS_URL)s %(VIRTUAL_ENV)s' % env)
    run('virtualenv %(VIRTUAL_ENV)s' % env)
    pull()
    with activate_virtualenv():
        run('easy_install -U distribute')
    install()
    with activate_virtualenv():
        run('./src/manage.py syncdb --all --noinput')
        run('./src/manage.py migrate --fake --noinput')

@task
def createsuperuser():
    "create a super user in the current stage"
    with activate_virtualenv():
        run('./src/manage.py createsuperuser')


@task
def pull():
    "pull the latest revision"
    with activate_virtualenv():
        run("git pull %(VCS_URL)s %(VCS_BRANCH)s" % env)


@task
def install():
    "istall all requirements with pip"
    with activate_virtualenv():
        run('pip install -r %(PIP_REQUIREMENTS)s' % env)


@task
def docs(clean=False):
    "update sphinx documents"
    with activate_virtualenv():
        with cd('docs'):
            if clean:
                run('make clean')
            run('make html')


@task
def syncdb():
    "sync and migrate the database"
    with activate_virtualenv():
        run('./src/manage.py syncdb --noinput')
        run('./src/manage.py migrate --noinput')


@task
def collectstatic():
    "copy all static files to a public place"
    with activate_virtualenv():
        #from fabric.contrib import django
        #django.settings_module(env.DJANGO_SETTINGS_MODULE) 
        #from django.conf import settings
        #run('rm -rf %s' % settings.STATIC_ROOT)
        run('./src/manage.py collectstatic --noinput -v 0')


@task
def restart():
    "kill the dynamic fcgi process to trigger a restart"
    with settings(warn_only=True):
        if exists(env.DJANGO_FCGI_PID) and \
           run('ps up `cat %(DJANGO_FCGI_PID)s` > /dev/null' % env).succeeded and \
           run('kill `cat %(DJANGO_FCGI_PID)s`' % env).succeeded:
            print "fcgi is running"
            return
    print "fcgi is not running"

@task(default=True)
def full_deploy():
    "run a full deploy"
    check()
    pull()
    install()
    syncdb()
    collectstatic()
    restart()
